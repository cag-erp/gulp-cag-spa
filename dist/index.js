'use strict';

var Through = require('through2'),
    Fs = require('fs-extra'),
    Path = require('path'),
    Slash = require('slash'),
    Glob = require("glob"),
    Lodash = require('lodash'),
    Async = require("async"),
    ReadYaml = require('read-yaml-promise'),
    MergeYaml = require('@alexlafroscia/yaml-merge'),
    JsYaml = require('js-yaml'),
    Winston = require("winston"),
    Babel = require('@babel/core'),
    Sass = require('node-sass'),
    Jsesc = require('jsesc'),
    cwd = Slash(process.cwd()),
    LineReader = require('line-reader'),
    PrettyBytes = require('pretty-bytes'),
    Plur = require('plur'),
    Minimist = require('minimist'),
    UglifyJS = require('uglify-js'),
    CamelCase = require('camelcase'),
    HtmlComments = require('html-comments'),
    RevPath = require('rev-path'),
    RunCommand = require('gulp-run-command').default,
    packageJson = Fs.readJsonSync(Slash(Path.join(cwd, 'package.json')));

var connect = require('gulp-connect'),
    imagemin = require('imagemin'),
    imageminJpegtran = require('imagemin-jpegtran'),
    imageminOptipng = require('imagemin-optipng'),
    imageminGifsicle = require('imagemin-gifsicle'),
    imageminSvgo = require('imagemin-svgo'),
    Plumber = require('gulp-plumber'),
    notify = require('gulp-notify'),
    watch = require('gulp-watch'),
    GulpIf = require('gulp-if'),
    Autoprefixer = require('autoprefixer'),
    Postcss = require('postcss');
/* inject:common:js */
var Constants = {
    SOURCE: {
        root: 'src/app',
        module: 'module',
        i18n: 'i18n',
        template: 'template',
        assets: 'assets',
        images: 'images',
        fonts: 'fonts',
        css: 'css',
        extJs: 'js'
    },

    RESOURCES: {
        root: 'src/resources',
        profile: 'profiles'
    },

    BUILD: {
        root: 'build',
        module: 'module',
        assets: 'assets',
        images: 'images',
        fonts: 'fonts',
        extJs: 'js'
    },

    DIST: {
        root: 'dist',
        assets: 'assets',
        images: 'images'
    },

    MODULE_TYPE: {
        StyleGuide: 'StyleGuide',
        MainApp: 'MainApp',
        Normal: 'Normal'
    }
};
var Format = Winston.format;

var logger = Winston.createLogger({
    format: Format.combine(Format(function (info) {
        info.level = info.level.toUpperCase();
        return info;
    })(), Format.colorize(), Format.timestamp({
        format: 'HH:mm:ss'
    }), Format.splat(), Format.simple(), Format.printf(function (info) {
        return '[' + info.timestamp + '] ' + info.level + ': ' + info.message;
    })),
    transports: [new Winston.transports.Console()]
});
function Notifier() {
    this.error = function (message) {
        notify({
            title: 'ERROR RUNNING - [' + resources.activedProfile.toUpperCase() + ']',
            icon: Path.join(__dirname, '..', 'assets', 'error.png'),
            message: workspace.moduleName + ': ' + message
        }).write({});
    };

    this.success = function (success) {
        return Through.obj(function (file, enc, done) {
            notify({
                title: 'BUILD SUCCESSFUL - [' + resources.activedProfile.toUpperCase() + ']',
                message: workspace.moduleName + ': ' + success,
                icon: Path.join(__dirname, '..', 'assets', 'success.png')
            }).write({});

            done(null, file);
        });
    };
}
function Location(moduleName, base, dir) {
    var self = this;
    var path = void 0;

    this.baseFolder = function () {
        path = '' + base;

        return self;
    };

    this.distFolder = function () {
        path = base + '/' + moduleName + '/dist';

        return self;
    };

    this.imgAssetFolder = function () {
        path = base + '/' + moduleName + '/dist/assets/images/' + moduleName;

        return self;
    };

    this.extJsAssetFolder = function () {
        path = base + '/' + moduleName + '/dist/assets/js/' + moduleName;

        return self;
    };

    this.fontAssetFolder = function () {
        path = base + '/' + moduleName + '/dist/assets/fonts/' + moduleName;

        return self;
    };

    this.fileName = function (fileName) {
        path = path + '/' + fileName;

        return self;
    };

    this.write = function (content) {
        return Fs.outputFile(dir + '/' + path, content);
    };

    this.copy = function (file) {
        return Fs.copy(file, dir + '/' + path);
    };

    this.cleanup = function () {
        return new Promise(function (resolve, reject) {
            return Glob(dir + '/' + path, function (err, files) {
                Async.each(files, Fs.remove, function (err) {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                });
            });
        });
    };

    this.path = function () {
        return path;
    };

    this.switchTo = function (name) {
        moduleName = name;

        return self;
    };
}
function Resources(options) {
    var self = this;

    var resourceFolderPath = 'src/resources',
        profileFolderPath = 'src/resources/profiles';

    this.getPath = function () {
        return resourceFolderPath;
    };

    this.getProfilePath = function () {
        return profileFolderPath;
    };

    this.load = function () {
        return Through.obj(function (file, enc, done) {
            self.activedProfile = options.P ? options.P : 'local';

            self.appYmlPath = resourceFolderPath + '/application.yaml', self.moduleYmlPath = resourceFolderPath + '/module.yaml', self.bootstrapYmlPath = resourceFolderPath + '/bootstrap.yaml', self.appProfileYmlPath = profileFolderPath + '/application.' + self.activedProfile + '.yaml', self.bootstrapProfileYmlPath = profileFolderPath + '/bootstrap.' + self.activedProfile + '.yaml';

            logger.info('The following profile is active: ' + self.activedProfile);

            done(null, file);
        });
    };

    this.getApplication = function () {
        return parsingYmlContent([self.appYmlPath, self.appProfileYmlPath]);
    };

    self.getModule = function () {
        return parsingYmlContent([self.moduleYmlPath]);
    };

    this.getBootstrap = function () {
        return parsingYmlContent([self.bootstrapYmlPath, self.bootstrapProfileYmlPath]);
    };

    this.isExists = function () {
        return Fs.pathExists(resourceFolderPath);
    };

    self.isAppYmlExists = function () {
        return Fs.existsSync(self.appYmlPath);
    };

    self.isModuleYmlExists = function () {
        return Fs.existsSync(self.moduleYmlPath);
    };

    function parsingYmlContent(paths) {
        var _this = this;

        return new Promise(function (resolve, reject) {
            Async.concat(paths, function (path, next) {
                Fs.pathExists(path).then(function (exists) {
                    if (exists) {
                        next(null, path);
                    } else {
                        next();
                    }
                });
            }, function (err, envYmlPaths) {
                if (err) {
                    reject(err);
                } else {
                    resolve(JsYaml.safeLoad(MergeYaml.apply(_this, envYmlPaths), 'utf8'));
                }
            });
        });
    }
}
function Service(options) {
    this.start = function () {
        return Through.obj(function (file, enc, done) {
            if (workspace.additionalBuild.indexHtml) {
                resources.getBootstrap().then(function (bootstrap) {
                    if (!bootstrap.settings) {
                        logger.error('SettingException: Setting file \'bootstrap.yaml\' does not exist or missing mandatory information!');

                        done();
                    } else {
                        var port = bootstrap.settings.www && bootstrap.settings.www.port ? bootstrap.settings.www.port : 8080;

                        connect.server({
                            name: workspace.moduleName,
                            root: [workspace.www().distFolder().path(), 'node_modules', workspace.location],
                            port: port
                        });

                        complete('Access URLs: http://localhost:' + port);

                        done(null, file);
                    }
                });
            } else {
                complete();

                done(null, file);
            }
        });

        function complete(message) {
            notify({
                title: 'Service \'' + workspace.moduleName + '\' is running! - [' + resources.activedProfile.toUpperCase() + ']',
                message: message ? message : ' ',
                icon: Path.join(__dirname, '..', 'assets', 'success.png')
            }).write({});
        }
    };
}
var SassVariables = {
    image: '$imgPath: "' + Constants.BUILD.assets + '/' + Constants.BUILD.images + '/' + packageJson.name + '";\n',
    font: '$fontPath: "' + Constants.BUILD.assets + '/' + Constants.BUILD.fonts + '/' + packageJson.name + '";\n'
};

function Variables() {
    var self = this;

    this.replaceAll = function (content) {
        return new Promise(function (resolve, reject) {
            Async.waterfall([Async.apply(replaceModuleName, content), replaceGlobalImgPath, replaceImgPath], function (err, result) {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        });
    };

    function replaceModuleName(content, done) {
        done(null, content.replace(/@@moduleName/g, '' + workspace.moduleName));
    }

    function replaceImgPath(content, done) {
        var replacement = workspace.locate().imgAssetFolder().path();

        done(null, content.replace(/@@imgPath/g, '' + replacement));
    }

    function replaceGlobalImgPath(content, done) {
        done(null, content.replace(/@@globalImgPath\[(.*)\]/g, function (match, moduleName, string) {
            return workspace.locate().switchTo(moduleName).imgAssetFolder().path();
        }));
    }
}
function Workspace() {
    var self = this;

    var localAppData = process.env.APPDATA || (process.platform == 'darwin' ? process.env.HOME + '/Library/Preferences' : process.env.HOME + '/.local/share');
    var rootFolderPath = localAppData + '/cag-platform/workspaces',
        referenceFolderPath = rootFolderPath + '/references',
        workspaceJsonPath = rootFolderPath + '/workspace.json',
        packageJsonPath = 'package.json';

    this.prepare = function () {
        return Through.obj(function (file, enc, done) {
            Promise.all([readJsonFile(workspaceJsonPath), readJsonFile(packageJsonPath)]).then(function (results) {
                var metadataList = results[0],
                    packageJson = results[1],
                    cwd = Slash(process.cwd()),
                    moduleDirectoryName = Path.dirname(cwd);

                if (!packageJson) {
                    logger.error('SettingException: \'' + packageJsonPath + '\' does not exist!');
                    done();
                }

                if (!metadataList) {
                    logger.error('SettingException: Unconfigured workspace settings!');
                    done();
                } else {
                    logger.info('Loading \'' + packageJson.name + ':' + packageJson.version + '\' workspace ...');

                    var metadata = metadataList.find(function (metadata) {
                        return metadata.location == moduleDirectoryName;
                    });

                    if (metadata) {
                        self.moduleName = packageJson.name;
                        self.moduleVersion = packageJson.version;
                        self.reference = metadata.reference;
                        self.location = referenceFolderPath;

                        done(null, file);
                    } else {
                        logger.error('SettingException: The \'' + packageJson.name + '\' workspace has not yet been configured or has obsolete already. Please double check with \'cag setup\' command!');
                        done();
                    }
                }
            });
        });
    };

    this.detectAdditionalBuild = function (settings, resources) {
        return Through.obj(function (file, enc, done) {
            self.additionalBuild = {};

            if (settings) {
                if (settings.additionalBuild) {
                    self.additionalBuild = Object.assign(settings.additionalBuild, self.additionalBuild);
                }

                self.onlyErrorNotification = settings.onlyErrorNotification ? settings.onlyErrorNotification : false;
            }

            resources.isExists().then(function (exists) {
                if (exists) {
                    var props = {
                        application: resources.isAppYmlExists(),
                        module: resources.isModuleYmlExists()
                    };

                    if (props.application) {
                        props.ico = true;
                        props.indexHtml = true;
                    }

                    return props;
                }
            }).then(function (additionalBuild) {
                if (additionalBuild) {
                    self.additionalBuild = Object.assign(additionalBuild, self.additionalBuild);
                }

                done(null, file);
            });
        });
    };

    self.detectAdditionalLocalSassModules = function (settings) {
        return Through.obj(function (file, enc, done) {

            self.additionalBuild.localSassModules = settings && settings.localSassModules ? settings.localSassModules : false;

            done(null, file);
        });
    };

    this.locate = function () {
        return new Location(self.moduleName, self.reference, self.location);
    };

    this.www = function () {
        return new Www();
    };

    function readJsonFile(path) {
        return Fs.pathExists(path).then(function (exists) {
            if (exists) {
                return Fs.readJson(path);
            }
        });
    };
}
function Www() {
    var self = this;
    var path = void 0;

    this.distFolder = function () {
        path = 'dist';

        return self;
    };

    this.fileName = function (fileName) {
        path = path + '/' + fileName;

        return self;
    };

    this.write = function (content) {
        return Fs.outputFile('' + path, content);
    };

    this.copy = function (file) {
        return Fs.copy(file, '' + path);
    };

    this.cleanup = function () {
        return new Promise(function (resolve, reject) {
            return Glob('' + path, function (err, files) {
                Async.each(files, Fs.remove, function (err) {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                });
            });
        });
    };

    this.path = function () {
        return path;
    };
}
function BlockManager() {
    var self = this;

    self.block = false;
    self.sections = {};
    self.sectionIndex = 0;
    self.last = null;
    self.removeBlockIndex = 0;
    self.sectionKey = null;

    this.getSectionKey = function (build) {
        var key = void 0;

        if (build.attbs) {
            key = [build.type, build.target, build.attbs].join(BlockResourcePattern.resources.sectionsJoinChar);
        } else if (build.target) {
            key = [build.type, build.target].join(BlockResourcePattern.resources.sectionsJoinChar);
        } else {
            key = build.type;
        }

        return key;
    };

    this.setSections = function (build) {
        if (build.type === 'remove') {
            build.target = String(self.removeBlockIndex++);
        }

        self.sectionKey = self.getSectionKey(build);

        if (self.sections[self.sectionKey]) {
            self.sectionKey += self.sectionIndex++;
        }

        self.sections[self.sectionKey] = self.last = [];
    };

    this.endbuild = function (line) {
        return BlockResourcePattern.resources.regend.test(line);
    };
}

function RefManager() {
    var self = this;

    self.parser = new BlockParser();
    self.bb = {};

    this.setBuildBlock = function (block, options) {
        var props = self.parser.parse(block),
            transformTargetPath = options.transformTargetPath;

        self.bb.handler = options && options[props.type];
        self.bb.target = props.target || 'replace';
        self.bb.type = props.type;
        self.bb.attbs = props.attbs;
        self.bb.alternateSearchPaths = props.alternateSearchPaths;

        // transform target file path
        if (typeof transformTargetPath === 'function') {
            self.bb.target = transformTargetPath(self.bb.target, self.bb.type);
        }
    };

    this.transformCSSRefs = function (block, target, attbs) {
        var ref = '',
            rel = 'rel="stylesheet" ';

        // css link element regular expression
        // TODO: Determine if 'href' attribute is present.
        var regcss = /<?link.*?(?:>|\))/gmi;

        // rel attribute regular expression
        var regrel = /(^|\s)rel=/i;

        // if rel exists in attributes, set the default one empty
        if (regrel.test(attbs)) {
            rel = '';
        }

        // Check to see if there are any css references at all.
        if (block.search(regcss) !== -1) {
            if (attbs) {
                ref = '<link ' + rel + 'href="' + target + '" ' + attbs + '>';
            } else {
                ref = '<link ' + rel + 'href="' + target + '">';
            }
        }

        return ref;
    };

    this.transformJSRefs = function (block, target, attbs) {
        var ref = '';

        // script element regular expression
        // TODO: Detect 'src' attribute.
        var regscript = /<?script\(?\b[^<]*(?:(?!<\/script>|\))<[^<]*)*(?:<\/script>|\))/gmi;

        // Check to see if there are any js references at all.
        if (block.search(regscript) !== -1) {
            if (attbs) {
                ref = '<script src="' + target + '" ' + attbs + '></script>';
            } else {
                ref = '<script src="' + target + '"></script>';
            }
        }

        return ref;
    };

    this.getRef = function (block, blockContent, options) {
        var ref = '';

        self.setBuildBlock(block, options);

        if (self.bb.type === 'css') {
            ref = self.transformCSSRefs(blockContent, self.bb.target, self.bb.attbs);
        } else if (self.bb.type === 'js') {
            ref = self.transformJSRefs(blockContent, self.bb.target, self.bb.attbs);
        } else if (self.bb.type === 'remove') {
            ref = '';
        } else if (self.bb.handler) {
            ref = self.bb.handler(blockContent, self.bb.target, self.bb.attbs, self.bb.alternateSearchPaths);
        } else {
            ref = null;
        }

        return ref;
    };
}
function BlockParser() {
    var self = this;

    this.parse = function (block) {
        var parts = block.match(BlockResourcePattern.resources.regbuild);

        return {
            type: parts[1],
            alternateSearchPaths: parts[2],
            target: parts[3] && parts[3].trim(),
            attbs: parts[4] && parts[4].trim()
        };
    };
}
function BlockResourcePattern() {}

BlockResourcePattern.resources = {};
//start build pattern: <!-- build:[target] output -->
// $1 is the type, $2 is the alternate search path, $3 is the destination file name $4 extra attributes
BlockResourcePattern.resources.regbuild = /(?:<!--|\/\/-)\s*build:(\w+)(?:\(([^)]+)\))?\s*([^\s]+(?=-->)|[^\s]+)?\s*(?:(.*))?\s*-->/;
// end build pattern -- <!-- endbuild -->
BlockResourcePattern.resources.regend = /(?:<!--|\/\/-)\s*endbuild\s*-->/;
// IE conditional comment pattern: $1 is the start tag and $2 is the end tag
BlockResourcePattern.resources.regcc = /(<!--\[if\s.*?\]>)[\s\S]*?(<!\[endif\]-->)/i;
// Character used to create key for the `sections` object. This should probably be done more elegantly.
BlockResourcePattern.resources.sectionsJoinChar = '\uE000';
function TransformReferences() {
    var self = this;

    this.transform = function (blocks, content, options) {
        var refm = new RefManager(),
            replaced = content;

        // Determine the linefeed from the content
        var linefeed = /\r\n/g.test(content) ? '\r\n' : '\n';

        // handle blocks
        Object.keys(blocks).forEach(function (key) {
            var block = blocks[key].join(linefeed),
                lines = block.split(linefeed),
                indent = (lines[0].match(/^\s*/) || [])[0],
                ccmatches = block.match(BlockResourcePattern.resources.regcc),
                blockContent = lines.slice(1, -1).join(linefeed),
                ref = refm.getRef(block, blockContent, options);

            if (ref !== null) {
                ref = indent + ref;

                // Reserve IE conditional comment if exist
                if (ccmatches) {
                    ref = indent + ccmatches[1] + linefeed + ref + linefeed + indent + ccmatches[2];
                }

                if (options.noconcat) {
                    replaced = replaced.replace(block, blockContent);
                } else {
                    replaced = replaced.replace(block, ref);
                }
            }
        });

        return replaced;
    };
}

function CompactContent() {
    var self = this,
        parser = new BlockParser();

    this.removeComments = function (lines) {
        return lines.join('\n').replace(BlockResourcePattern.resources.regComment, '').split('\n');
    };

    this.compact = function (blocks, options) {
        var result = {},
            parseSourcePath = options.parseSourcePath || function (tag) {
            return (tag.match(/(href|src)=(?:["']\W+\s*(?:\w+)\()?["']([^'"]+)['"]/) || [])[2];
        };

        Object.keys(blocks).forEach(function (dest) {
            // Lines are the included scripts w/o the use blocks
            var lines = blocks[dest].slice(1, -1),
                parts = dest.split(BlockResourcePattern.resources.sectionsJoinChar),
                type = parts[0],


            // output is the useref block file
            output = parts[1],
                build = parser.parse(blocks[dest][0]),
                assets = void 0;

            // remove html comment blocks
            lines = self.removeComments(lines);

            // parse out the list of assets to handle, and update the config accordingly
            assets = lines.map(function (tag) {
                if (typeof parseSourcePath !== 'function') {
                    throw new Error('options.parseSourcePath must be a function');
                }

                // call function to parse the asset path
                return parseSourcePath(tag, type);
            }).reduce(function (a, b) {
                return b ? a.concat(b) : a;
            }, []);

            result[type] = result[type] || {};

            result[type][output] = {
                assets: assets
            };

            if (build.alternateSearchPaths) {
                // Alternate search path
                result[type][output].searchPaths = build.alternateSearchPaths;
            }
        });

        return result;
    };
}
function UseRef() {
    var self = this;

    this.replace = function (content, options) {
        var lines = content.replace(/\r\n/g, '\n').split(/\n/),
            bbm = new BlockManager(),
            parser = new BlockParser();

        bbm.sections = {};

        lines.forEach(function (l) {
            if (BlockResourcePattern.resources.regbuild.test(l)) {
                bbm.block = true;

                bbm.setSections(parser.parse(l));
            }

            if (bbm.block && bbm.last) {
                bbm.last.push(l);
            }

            if (bbm.block && bbm.endbuild(l)) {
                bbm.block = false;
            }
        });

        var blocks = bbm.sections,
            opts = options || {},
            transformReferences = new TransformReferences(),
            compactContent = new CompactContent();

        var transformedContent = transformReferences.transform(blocks, content, opts),
            replaced = compactContent.compact(blocks, opts);

        return {
            content: transformedContent,
            block: replaced
        };
    };
}

/* end:inject:common */

var options = Minimist(process.argv.slice(2)),
    workspace = new Workspace(options),
    resources = new Resources(options),
    notifier = new Notifier(),
    useref = new UseRef(),
    variables = new Variables();

var i18nBuilder = new I18nBuilder(options),
    cleanup = new Cleanup(options),
    moduleBuilder = new ModuleBuilder(options),
    cssBuilder = new CssBuilder(options),
    templateBuilder = new TemplateBuilder(options),
    indexBuilder = new IndexBuilder(options),
    assetBuilder = new AssetBuilder(options),
    applicationBuilder = new ApplicationBuilder(options),
    optimizationBuilder = new OptimizationBuilder(options);

/* inject:build:js */
function ApplicationBuilder() {
    var self = this;

    this.buildApplicationConstants = function () {
        return Through.obj(function (file, enc, done) {
            resources.getApplication().then(buildConstants).then(function (err) {
                if (err) {
                    done(err);
                } else {
                    done(null, file);
                }
            });
        });
    };

    self.buildModuleConstants = function () {
        return Through.obj(function (file, enc, done) {
            resources.getModule().then(buildConstants).then(function (err) {
                if (err) {
                    done(err);
                } else {
                    done(null, file);
                }
            });
        });
    };

    this.cleanup = function () {
        return Through.obj(function (file, enc, done) {
            cleanup(function (err) {
                if (err) {
                    done(err);
                } else {
                    done(null, file);
                }
            });
        });
    };

    function cleanup(done) {
        logger.info('Start cleaning up previous build...');

        return workspace.locate().distFolder().fileName('*.constants.js').cleanup().then(function (err) {
            if (err) {
                done(err);
            } else {
                logger.info('The previous build had been cleaned up completely!');
                done();
            }
        });
    };

    function buildConstants(props) {
        return new Promise(function (resolve, reject) {
            logger.info('Start building constants ...');

            var TEMPLATE_CONTAINER = 'angular.module(\'<%= module %>\'<%= standalone %>)';
            var TEMPLATE_BODY = '.constant(\'<%= name %>\', <%= value %>)';
            var compiledTemplateContainer = Lodash.template(TEMPLATE_CONTAINER);
            var compiledTemplateBody = Lodash.template(TEMPLATE_BODY);
            var results = [];

            results.push(compiledTemplateContainer({
                module: workspace.moduleName,
                standalone: ''
            }));

            Object.keys(props).forEach(function (key) {
                results.push(compiledTemplateBody({
                    name: key,
                    value: JSON.stringify(props[key])
                }));
            });

            var fileName = workspace.moduleName + '.constants.js';

            workspace.locate().distFolder().fileName(fileName).write(results.join('\r\n') + ';').then(function (err) {
                if (err) {
                    reject(err);
                } else {
                    logger.info("'%s' has been generated successfully", fileName);
                    resolve();
                }
            });
        });
    }
}
function AssetBuilder(options) {
    var self = this;

    this.getImagePath = function () {
        return Constants.SOURCE.root + '/' + Constants.SOURCE.assets + '/' + Constants.SOURCE.images;
    };

    this.getFontPath = function () {
        return Constants.SOURCE.root + '/' + Constants.SOURCE.assets + '/' + Constants.SOURCE.fonts;
    };

    this.getExtJsPath = function () {
        return Constants.SOURCE.root + '/' + Constants.SOURCE.assets + '/' + Constants.SOURCE.extJs;
    };

    this.buildImage = function () {
        return Through.obj(function (file, enc, done) {
            buildImage(function (err) {
                if (err) {
                    done(err);
                } else {
                    done(null, file);
                }
            });
        });
    };

    this.buildExtJs = function () {
        return Through.obj(function (file, enc, done) {
            buildExtJs(function (err) {
                if (err) {
                    done(err);
                } else {
                    done(null, file);
                }
            });
        });
    };

    this.buildFont = function () {
        return Through.obj(function (file, enc, done) {
            buildFont(function (err) {
                if (err) {
                    done(err);
                } else {
                    done(null, file);
                }
            });
        });
    };

    this.cleanupExtJs = function () {
        return Through.obj(function (file, enc, done) {
            logger.info('Start cleaning up previous build...');

            return workspace.locate().extJsAssetFolder().fileName('*').cleanup().then(function (err) {
                if (err) {
                    done(err);
                } else {
                    logger.info('The previous build had been cleaned up completely!');
                    done(null, file);
                }
            });
        });
    };

    this.cleanupImage = function () {
        return Through.obj(function (file, enc, done) {
            logger.info('Start cleaning up previous build...');

            return workspace.locate().imgAssetFolder().fileName('*').cleanup().then(function (err) {
                if (err) {
                    done(err);
                } else {
                    logger.info('The previous build had been cleaned up completely!');
                    done(null, file);
                }
            });
        });
    };

    this.cleanupFont = function () {
        return Through.obj(function (file, enc, done) {
            logger.info('Start cleaning up previous build...');

            return workspace.locate().fontAssetFolder().fileName('*').cleanup().then(function (err) {
                if (err) {
                    done(err);
                } else {
                    logger.info('The previous build had been cleaned up completely!');
                    done(null, file);
                }
            });
        });
    };

    function buildImage(done) {
        logger.info('Start building image files ...');

        var plugins = [imageminJpegtran(), imageminOptipng(), imageminGifsicle(), imageminSvgo()];

        var imagePath = self.getImagePath();

        return Glob(imagePath + '/**/*.{jpg,png,svg}', function (err, files) {
            var totalBytes = 0;
            var totalSavedBytes = 0;
            var totalFiles = 0;

            return Async.eachSeries(files, function (file, next) {
                return Fs.readFile(file, function (err, original) {
                    if (err) {
                        done(err);
                    }

                    var fileName = file.replace(imagePath, '');

                    imagemin.buffer(original, { plugins: plugins }).then(function (optimized) {
                        var originalSize = original.length;
                        var optimizedSize = optimized.length;
                        var saved = originalSize - optimizedSize;
                        var percent = originalSize > 0 ? saved / originalSize * 100 : 0;

                        var savedMsg = 'saved ' + PrettyBytes(saved) + ' - ' + percent.toFixed(1).replace(/\.0$/, '') + '%';
                        var msg = saved > 0 ? savedMsg : 'already optimized';

                        if (saved > 0) {
                            totalBytes += originalSize;
                            totalSavedBytes += saved;
                            totalFiles++;
                        }

                        workspace.locate().imgAssetFolder().fileName(fileName).write(optimized).then(function (err) {
                            if (err) {
                                next(err);
                            } else {
                                next();
                            }
                        });
                    });
                });
            }, function (err) {
                if (err) {
                    done(err);
                }

                var percent = totalBytes > 0 ? totalSavedBytes / totalBytes * 100 : 0;
                var msg = 'Minified ' + totalFiles + ' ' + Plur('image', totalFiles);

                if (totalFiles > 0) {
                    msg += ' (saved ' + PrettyBytes(totalSavedBytes) + ' - ' + percent.toFixed(1).replace(/\.0$/, '') + '%)';
                }

                logger.info(msg);

                done();
            });
        });
    }

    function buildExtJs(done) {
        logger.info('Start building Ext JS files ...');

        var extJsPath = self.getExtJsPath();

        return Glob(extJsPath + '/**/*.js', function (err, files) {
            return Async.eachSeries(files, function (file, next) {
                var fileName = Path.basename(file);

                workspace.locate().extJsAssetFolder().fileName(fileName).copy(file).then(function (err) {
                    if (err) {
                        next(err);
                    } else {
                        next();
                    }
                });
            }, function (err) {
                if (err) {
                    done(err);
                } else {
                    logger.info('Ext JS files had been built completely!');

                    done();
                }
            });
        });
    }

    function buildFont(done) {
        logger.info('Start building font files ...');

        var fontPath = self.getFontPath();

        return Glob(fontPath + '/**/*.{ttf,eot,svg,woff}', function (err, files) {
            return Async.eachSeries(files, function (file, next) {
                var fileName = Path.basename(file);

                workspace.locate().fontAssetFolder().fileName(fileName).copy(file).then(function (err) {
                    if (err) {
                        next(err);
                    } else {
                        next();
                    }
                });
            }, function (err) {
                if (err) {
                    done(err);
                } else {
                    logger.info('Font files had been copied completely!');

                    done();
                }
            });
        });
    };
}
function CssBuilder(options) {
    var self = this;

    this.getPath = function () {
        return Slash(Path.join(Constants.SOURCE.root, Constants.SOURCE.assets, Constants.SOURCE.css));
    };

    this.all = function () {
        return Through.obj(function (file, enc, done) {
            Async.waterfall([cleanup, buildScss], function (err, result) {
                if (err) {
                    done(err);
                } else {
                    done(null, file);
                }
            });
        });
    };

    this.buildScss = function () {
        return Through.obj(function (file, enc, done) {
            buildScss(function (err) {
                if (err) {
                    done(err);
                } else {
                    done(null, file);
                }
            });
        });
    };

    function cleanup(done) {
        logger.info('Start cleaning up previous build...');

        return workspace.locate().distFolder().fileName(workspace.moduleName + '.css').cleanup().then(function (err) {
            if (err) {
                done(err);
            } else {
                logger.info('The previous build had been cleaned up completely!');
                done();
            }
        });
    };

    function buildScss(done) {
        logger.info('Start building scss files ...');

        var cssPath = self.getPath();
        return Fs.pathExists(cssPath).then(function (exists) {
            if (exists) {
                return Glob(cssPath + '/**/main.scss', function (err, files) {
                    if (files && files.length == 1) {
                        var main = files[0];

                        return Fs.readFile(main, function (err, data) {
                            var additionalVariables = Buffer.from('' + SassVariables.image + SassVariables.font);
                            var buffer = Buffer.concat([additionalVariables, data], additionalVariables.length + data.length);
                            var includePaths = [];

                            includePaths.push(cwd + '/node_modules');
                            includePaths.push('' + cssPath);

                            if (workspace.additionalBuild.localSassModules) {
                                workspace.additionalBuild.localSassModules.forEach(function (each) {
                                    includePaths.push(Slash(Path.resolve(cwd, '..') + '/' + each + '/' + self.getPath()));
                                });
                            }

                            Sass.render({
                                data: buffer.toString(),
                                includePaths: includePaths
                            }, function (err, result) {
                                if (err) {
                                    done(err);
                                } else {
                                    var fileName = workspace.moduleName + '.css';

                                    Postcss([Autoprefixer]).process(result.css, { from: undefined }).then(function (result_after_prefix) {
                                        result_after_prefix.warnings().forEach(function (warn) {
                                            logger.warn(warn.toString());
                                        });

                                        workspace.locate().distFolder().fileName(fileName).write(result_after_prefix.css).then(function (err) {
                                            if (err) {
                                                done(err);
                                            } else {
                                                logger.info("'%s' has been generated successfully", fileName);
                                                done();
                                            }
                                        });
                                    });
                                }
                            });
                        });
                    } else {
                        done();
                    }
                });
            } else {
                done();
            }
        });
    }
}
function I18nBuilder(options) {
    var self = this;

    this.getPath = function () {
        return Constants.SOURCE.root + '/' + Constants.SOURCE.i18n;
    };

    this.all = function () {
        return Through.obj(function (file, enc, done) {
            Async.waterfall([cleanup, buildI18n], function (err, result) {
                if (err) {
                    done(err);
                } else {
                    done(null, file);
                }
            });
        });
    };

    this.buildI18n = function () {
        return Through.obj(function (file, enc, done) {
            buildI18n(function (err) {
                if (err) {
                    done(err);
                } else {
                    done(null, file);
                }
            });
        });
    };

    function cleanup(done) {
        logger.info('Start cleaning up previous build...');

        return workspace.locate().distFolder().fileName('*.translate.js').cleanup().then(function (err) {
            if (err) {
                done(err);
            } else {
                logger.info('The previous build had been cleaned up completely!');
                done();
            }
        });
    };

    function buildI18n(done) {
        logger.info('Start building i18n files ...');

        var i18nPath = self.getPath();
        return Fs.pathExists(i18nPath).then(function (exists) {
            if (exists) {
                return Fs.readdir(i18nPath).then(function (dirs) {
                    var map = {};

                    dirs.forEach(function (dir) {
                        map[dir] = Slash(Path.join(cwd, i18nPath, dir));
                    });

                    Async.mapValues(map, function (file, key, next) {
                        parseMessage(file, key, next);
                    }, function (err, result) {
                        var compiledTranslation = Lodash.template('$translateProvider.translations("<%= language %>", <%= contents %>);\n');
                        var compiledModule = Lodash.template('angular.module("<%= module %>"<%= standalone %>).config(["$translateProvider", function($translateProvider) {\n<%= contents %>}]);\n');
                        var translations = '';

                        Lodash.keyBy(dirs, function (key) {
                            logger.info("Found '%s' language ...", key);
                            translations += compiledTranslation({
                                language: key,
                                contents: JSON.stringify(result[key])
                            });
                        });

                        var fileName = workspace.moduleName + '.translate.js';

                        workspace.locate().distFolder().fileName(fileName).write(compiledModule({
                            contents: translations,
                            module: workspace.moduleName,
                            standalone: ''
                        })).then(function (err) {
                            if (err) {
                                done(err);
                            } else {
                                logger.info("'%s' has been generated successfully!", fileName);
                                done();
                            }
                        });
                    });
                });
            } else {
                done();
            }
        });
    };

    function parseMessage(dir, key, done) {
        return Glob(Slash(Path.join(dir, '**', '*.yaml')), function (err, files) {
            var map = {};

            files.forEach(function (file) {
                map[Path.parse(file).name] = file;
            });

            Async.mapValues(map, function (file, key, next) {
                return ReadYaml(file).then(function (data) {
                    return next(null, data);
                });
            }, function (err, result) {
                done(null, result);
            });
        });
    };
}
function IndexBuilder() {
    var self = this;

    var SCRIPT_TAG_PATTERN = new RegExp('<!-- scriptTags -->'),
        LINK_TAG_PATTERN = new RegExp('<!-- linkTags -->'),
        SCRIPT_TAG_TEMPLATE = '<script src="<%= content %>"></script>',
        LINK_TAG_TEMPLATE = '<link rel="stylesheet" href="<%= content %>"/>';

    this.getPath = function () {
        return Constants.SOURCE.root;
    };

    this.cleanup = function () {
        return Through.obj(function (file, enc, done) {
            cleanup(function (err) {
                if (err) {
                    done(err);
                } else {
                    done(null, file);
                }
            });
        });
    };

    this.buildIco = function () {
        return Through.obj(function (file, enc, done) {
            buildIco(function (err) {
                if (err) {
                    done(err);
                } else {
                    done(null, file);
                }
            });
        });
    };

    this.buildIndexHtml = function () {
        return Through.obj(function (file, enc, done) {
            resources.getBootstrap().then(buildIndexHtml).then(function (err) {
                if (err) {
                    done(err);
                } else {
                    done(null, file);
                }
            });
        });
    };

    function cleanup(done) {
        logger.info('Start cleaning up previous build...');

        workspace.www().distFolder().fileName('*.{ico,html}').cleanup().then(function (err) {
            if (err) {
                done(err);
            } else {
                logger.info('The previous build had been cleaned up completely!');
                done();
            }
        });
    };

    function buildIco(done) {
        logger.info('Start building ico files ...');

        var path = self.getPath();
        var destDir = Constants.DIST.root;

        return Glob(path + '/**/*.ico', function (err, files) {
            return Async.eachSeries(files, function (file, next) {
                var fileName = Path.basename(file);

                workspace.www().distFolder().fileName(fileName).copy(file).then(next);
            }, function (err) {
                if (err) {
                    done(err);
                } else {
                    logger.info('Ico files had been built completely!');

                    done();
                }
            });
        });
    };

    function buildIndexHtml(bootstrap) {
        return new Promise(function (resolve, reject) {
            if (!bootstrap.dependencies) {
                reject('SettingException: Setting file \'bootstrap.yaml\' does not exist or missing mandatory information!');
            } else {
                var injectScriptTags = [];
                var injectLinkTags = [];

                Object.keys(bootstrap.dependencies).forEach(function (key) {
                    var dependency = bootstrap.dependencies[key];
                    if (dependency && dependency.js) {
                        dependency.js.forEach(function (file) {
                            injectScriptTags.push(dependency.local ? workspace.locate().baseFolder().fileName('' + file).path() : '' + file);
                        });
                    }

                    if (dependency && dependency.css) {
                        dependency.css.forEach(function (file) {
                            injectLinkTags.push(dependency.local ? workspace.locate().baseFolder().fileName('' + file).path() : '' + file);
                        });
                    }
                });

                var indexFilePath = self.getPath() + '/index.html',
                    compiledScriptTagTemplateBody = Lodash.template(SCRIPT_TAG_TEMPLATE),
                    compiledLinkTagTemplateBody = Lodash.template(LINK_TAG_TEMPLATE),
                    content = [];

                return LineReader.eachLine(indexFilePath, function (line, last) {
                    content.push(line);

                    if (SCRIPT_TAG_PATTERN.test(line)) {
                        var spaces = line.match(/^\s{0,}\B/)[0];
                        injectScriptTags.forEach(function (inject) {
                            var tag = compiledScriptTagTemplateBody({
                                content: inject
                            });

                            content.push('' + spaces + tag);
                        });
                    }

                    if (LINK_TAG_PATTERN.test(line)) {
                        var _spaces = line.match(/^\s{0,}\B/)[0];
                        injectLinkTags.forEach(function (inject) {
                            var tag = compiledLinkTagTemplateBody({
                                content: inject
                            });

                            content.push('' + _spaces + tag);
                        });
                    }

                    if (last) {
                        var fileName = 'index.html';

                        variables.replaceAll(content.join('\r\n')).then(function (content) {
                            workspace.www().distFolder().fileName(fileName).write(content).then(function (err) {
                                if (err) {
                                    reject(err);
                                } else {
                                    logger.info("'%s' has been compiled successfully", fileName);

                                    resolve();
                                }
                            });
                        });
                    }
                });
            }
        });
    }
}
function ModuleBuilder(options) {
    var self = this;

    this.getPath = function () {
        return Constants.SOURCE.root + '/' + Constants.SOURCE.module;
    };

    this.all = function () {
        return Through.obj(function (file, enc, done) {
            Async.waterfall([cleanup, buildModule], function (err, result) {
                if (err) {
                    done(err);
                } else {
                    done(null, file);
                }
            });
        });
    };

    this.buildModule = function () {
        return Through.obj(function (file, enc, done) {
            buildModule(function (err) {
                if (err) {
                    done(err);
                } else {
                    done(null, file);
                }
            });
        });
    };

    function cleanup(done) {
        logger.info('Start cleaning up previous build...');

        return workspace.locate().distFolder().fileName(workspace.moduleName + '.js').cleanup().then(function (err) {
            if (err) {
                done(err);
            } else {
                logger.info('The previous build had been cleaned up completely!');
                done();
            }
        });
    };

    function buildModule(done) {
        logger.info('Start building module js files ...');

        var modulePath = self.getPath();
        return Fs.pathExists(modulePath).then(function (exists) {
            if (exists) {
                logger.info('Compiling JS files ...');

                var hasError = false;

                return Glob(Slash(modulePath + '/**/*.js'), function (err, files) {
                    Async.concat(files, function (file, next) {
                        Babel.transformFile(file, {}, function (err, result) {
                            if (err) {
                                logger.error(err);
                                hasError = true;
                                next(null);
                            } else {
                                variables.replaceAll(result.code).then(function (content) {
                                    next(null, content);
                                });
                            }
                        });
                    }, function (err, contents) {
                        if (err) {
                            done(err);
                        } else if (hasError) {
                            done('Module js files have been compiled failed !!!');
                        } else {
                            var content = contents.join('\n');
                            var minifiedContent = void 0;

                            if (options.m) {
                                minifiedContent = UglifyJS.minify(content);

                                if (minifiedContent.error) {
                                    done(minifiedContent.error);
                                } else {
                                    writeToFile(minifiedContent.code, done);
                                }
                            } else {
                                writeToFile(content, done);
                            }
                        }
                    });
                });
            } else {
                done();
            }
        });
    };

    function writeToFile(content, done) {
        var fileName = workspace.moduleName + '.js';

        workspace.locate().distFolder().fileName(fileName).write(content).then(function (err) {
            if (err) {
                done(err);
            } else {
                logger.info("'%s' has been generated successfully", fileName);
                done();
            }
        });
    }
}

function OptimizationBuilder() {
    var self = this;

    this.optimizeIndexHtml = function (settings, options) {
        return Through.obj(function (file, enc, done) {
            optimizeIndexHtml(settings, options, function (err) {
                if (err) {
                    done(err);
                } else {
                    done(null, file);
                }
            });
        });
    };

    this.optimizeBuild = function (settings, options) {
        return Through.obj(function (file, enc, done) {
            optimizeBuild(settings, options, function (err) {
                if (err) {
                    done(err);
                } else {
                    done(null, file);
                }
            });
        });
    };

    function optimizeBuild(settings, options, done) {
        logger.info('Start optimizing build resources...');

        Async.waterfall([Async.apply(revAssets, settings, options), Async.apply(replaceAssets, settings, options)], function (err, result) {
            done(err);
        });
    }

    function replaceAssets(settings, options, revisions, done) {
        return workspace.getModuleLocation(packageJson.name).then(function (location) {
            var resourceDir = location + '/' + Constants.DIST.root;

            return Glob(resourceDir + '/*.{js,css}', function (err, files) {
                Async.each(files, function (file, next) {
                    replaceContent(file, revisions, next);
                }, done);
            });
        });
    }

    function replaceContent(file, revisions, done) {
        Fs.readFile(file, function (err, data) {
            if (err) {
                done(err);
            } else {
                var content = data.toString('utf8');
                Async.each(revisions, function (revision, next) {
                    content = content.replace(revision.original, revision.newest);
                    next();
                }, function (err) {
                    if (err) {
                        done(err);
                    } else {
                        Fs.outputFile(file, content).then(done);
                    }
                });
            }
        });
    }

    function revAssets(settings, options, done) {
        return workspace.getModuleLocation(packageJson.name).then(function (location) {
            var assetDir = location + '/' + Constants.DIST.root + '/' + Constants.DIST.assets;
            var hash = packageJson.version;

            return Glob(assetDir + '/**/*.*', function (err, files) {
                Async.concat(files, function (file, next) {
                    Fs.readFile(file, function (err, data) {
                        if (err) {
                            next(err);
                        } else {
                            Fs.rename(file, RevPath(file, hash), function (err) {
                                if (err) {
                                    next(err);
                                } else {
                                    var relativePath = file.replace(assetDir + '/', '');

                                    next(null, {
                                        original: relativePath,
                                        newest: Slash(RevPath(file.replace(assetDir + '/', ''), hash))
                                    });
                                }
                            });
                        }
                    });
                }, function (err, contents) {
                    if (err) {
                        done(err);
                    } else {
                        done(null, contents);
                    }
                });
            });
        });
    }

    function optimizeIndexHtml(settings, options, done) {
        logger.info('Start optimizing index.html\'s resources...');

        var indexFileName = 'index.html';
        var path = Constants.DIST.root + '/' + indexFileName;
        return Fs.pathExists(path).then(function (exists) {
            if (!exists) {
                logger.warn('index.html file is not available!');
            } else {
                return Fs.readFile(path, function (err, data) {
                    if (err) {
                        done(err);
                    } else {
                        var content = data.toString('utf8');
                        var replaced = useref.replace(content);

                        Async.waterfall([Async.apply(generate, replaced.block.js), Async.apply(generate, replaced.block.css)], function (err, result) {
                            if (err) {
                                done(err);
                            } else {
                                return Fs.outputFile(Constants.DIST.root + '/' + indexFileName, replaced.content).then(function () {
                                    logger.info("'%s' has been optimized successfully", indexFileName);

                                    done();
                                });
                            }
                        });
                    }
                });
            }
        });
    }

    function generate(resources, done) {
        var buildFiles = Object.keys(resources);

        Async.each(buildFiles, function (buildFile, next) {
            concat(buildFile, resources[buildFile].assets, function (err) {
                if (err) {
                    next(err);
                } else {
                    next();
                }
            });
        }, function (err) {
            if (err) {
                done(err);
            } else {
                done();
            }
        });
    }

    function concat(buildFile, resources, done) {
        workspace.getLocation(true).then(function (location) {
            var locations = ['node_modules', location];

            Async.concat(resources, function (resource, next) {
                getPath(resource, locations, function (err, path) {
                    if (err) {
                        next(err);
                    } else {
                        Fs.readFile(path, function (err, data) {
                            if (err) {
                                next(err);
                            } else {
                                next(null, data.toString('utf8'));
                            }
                        });
                    }
                });
            }, function (err, contents) {
                if (err) {
                    done(err);
                } else {
                    return Fs.outputFile(Constants.DIST.root + '/' + buildFile, contents.join('\n')).then(function () {
                        logger.info("'%s' has been concat successfully", buildFile);

                        done();
                    });
                }
            });
        });
    }

    function getPath(resource, locations, done) {
        Async.filter(locations, function (location, next) {
            Fs.pathExists(location + '/' + resource).then(function (exist) {
                return next(null, exist);
            });
        }, function (err, results) {
            if (err) {
                done(err);
            } else {
                done(null, results[0] + '/' + resource);
            }
        });
    }
}
function TemplateBuilder() {
    var self = this;

    this.getPath = function () {
        return Slash(Path.join(Constants.SOURCE.root, Constants.SOURCE.template));
    };

    this.all = function () {
        return Through.obj(function (file, enc, done) {
            var tasks = [cleanup, buildTemplateJs];

            if (workspace.additionalBuild.templateConstant) {
                tasks.push(buildTemplateConstant);
            }

            Async.waterfall(tasks, function (err, result) {
                if (err) {
                    done(err);
                } else {
                    done(null, file);
                }
            });
        });
    };

    this.buildTemplateJs = function () {
        return Through.obj(function (file, enc, done) {
            buildTemplateJs(function (err) {
                if (err) {
                    done(err);
                } else {
                    done(null, file);
                }
            });
        });
    };

    this.buildTemplateConstant = function () {
        return Through.obj(function (file, enc, done) {
            buildTemplateConstant(function (err) {
                if (err) {
                    done(err);
                } else {
                    done(null, file);
                }
            });
        });
    };

    function cleanup(done) {
        logger.info('Start cleaning up previous build...');

        return workspace.locate().distFolder().fileName('*.template.js').cleanup().then(function (err) {
            if (err) {
                done(err);
            } else {
                logger.info('The previous build had been cleaned up completely!');
                done();
            }
        });
    };

    function buildTemplateJs(done) {
        logger.info('Start building template js ...');

        var TEMPLATE_CONTAINER = 'angular.module(\'<%= module %>\'<%= standalone %>).run([\'$templateCache\', function($templateCache) {\n<%= body %>\n}]);';
        var TEMPLATE_BODY = '$templateCache.put(\'<%= url %>\', \'<%= contents %>\');';
        var moduleTemplatePath = self.getPath();

        var compiledTemplateContainer = Lodash.template(TEMPLATE_CONTAINER);
        var compiledTemplateBody = Lodash.template(TEMPLATE_BODY);

        return Glob(moduleTemplatePath + '/**/*.html', function (err, files) {
            Async.concat(files, function (file, next) {
                Fs.readFile(file, function (err, data) {
                    if (err) {
                        next(err);
                    } else {
                        variables.replaceAll(data.toString('utf8')).then(function (content) {
                            next(null, compiledTemplateBody({
                                contents: Jsesc(content),
                                url: file.replace(moduleTemplatePath, workspace.moduleName),
                                standalone: ''
                            }));
                        });
                    }
                });
            }, function (err, contents) {
                if (err) {
                    done(err);
                } else {
                    var fileName = workspace.moduleName + '.template.js';

                    workspace.locate().distFolder().fileName(fileName).write(compiledTemplateContainer({
                        module: workspace.moduleName,
                        body: contents.join('\n'),
                        standalone: ''
                    })).then(function (err) {
                        if (err) {
                            done(err);
                        } else {
                            logger.info("'%s' has been generated successfully", fileName);
                            done();
                        }
                    });
                }
            });
        });
    };

    function buildTemplateConstant(done) {
        logger.info('Start building template constant ...');

        var TEMPLATE_CONTAINER = 'angular.module(\'<%= module %>\'<%= standalone %>)';
        var TEMPLATE_BODY = '\r\n.constant(\'<%= name %>\', <%= value %>);';
        var moduleTemplatePath = self.getPath();

        var pattern = workspace.additionalBuild.templateConstantPattern ? workspace.additionalBuild.templateConstantPattern : '/**/*.html';

        return Glob('' + moduleTemplatePath + pattern, function (err, files) {
            Async.concat(files, function (file, next) {
                Fs.readFile(file, function (err, data) {
                    if (err) {
                        next(err);
                    } else {
                        var content = data.toString('utf8');

                        var brief = HtmlComments.load(content, {
                            keyword: 'Brief:',
                            removeKeyword: true
                        });

                        var scripts = HtmlComments.load(content, {
                            keyword: 'script:',
                            removeKeyword: true
                        });

                        var response = {
                            name: Path.basename(file),
                            path: workspace.moduleName + file.replace(moduleTemplatePath, ''),
                            brief: brief && brief.length > 0 ? brief[0] : '',
                            scripts: []
                        };

                        scripts.forEach(function (script) {
                            response.scripts.push(workspace.locate().extJsAssetFolder().fileName(script.trim()).path());
                        });

                        next(null, response);
                    }
                });
            }, function (err, constants) {
                if (err) {
                    done(err);
                } else {
                    var compiledTemplateContainer = Lodash.template(TEMPLATE_CONTAINER);
                    var compiledTemplateBody = Lodash.template(TEMPLATE_BODY);

                    var container = compiledTemplateContainer({
                        module: workspace.moduleName,
                        standalone: ''
                    });

                    var constantName = CamelCase(workspace.moduleName + 'TemplateConstant');

                    var body = compiledTemplateBody({
                        name: constantName,
                        value: JSON.stringify(constants)
                    });

                    var fileName = workspace.moduleName + '.constant.template.js';

                    workspace.locate().distFolder().fileName(fileName).write(container + body).then(function (err) {
                        if (err) {
                            done(err);
                        } else {
                            logger.info("'%s' has been generated successfully", fileName);
                            done();
                        }
                    });
                }
            });
        });
    };
}
function Cleanup(options) {
    this.all = function () {
        return Through.obj(function (file, enc, done) {
            logger.info('Start cleaning all previous build...');

            return workspace.locate().distFolder().fileName('*.*').cleanup().then(function (err) {
                if (err) {
                    done(err);
                } else {
                    logger.info('All previous build had been cleaned up completely!');
                    done(null, file);
                }
            });
        });
    };
}
/* end:inject:build */

function GulpCagSPa(gulp) {
    this.init = function (settings) {
        gulp.task('init', function () {
            return gulp.src('./package.json').pipe(resources.load()).pipe(workspace.prepare()).pipe(workspace.detectAdditionalBuild(settings, resources)).pipe(workspace.detectAdditionalLocalSassModules(settings));
        });

        gulp.task('build', function () {
            return gulp.src('./package.json').pipe(cleanup.all()).pipe(i18nBuilder.buildI18n()).pipe(moduleBuilder.buildModule()).pipe(cssBuilder.buildScss()).pipe(templateBuilder.buildTemplateJs()).pipe(GulpIf(workspace.additionalBuild.templateConstant, templateBuilder.buildTemplateConstant())).pipe(assetBuilder.buildImage()).pipe(GulpIf(workspace.additionalBuild.extJs, assetBuilder.buildExtJs())).pipe(assetBuilder.buildFont()).pipe(GulpIf(workspace.additionalBuild.application, applicationBuilder.buildApplicationConstants())).pipe(GulpIf(workspace.additionalBuild.module, applicationBuilder.buildModuleConstants())).pipe(GulpIf(workspace.additionalBuild.ico, indexBuilder.buildIco())).pipe(GulpIf(workspace.additionalBuild.indexHtml, indexBuilder.buildIndexHtml()));
        });

        gulp.task('build-i18n', function () {
            return gulp.src('./package.json').pipe(Plumber({
                errorHandler: function errorHandler(error) {
                    notifier.error(error);
                }
            })).pipe(i18nBuilder.all()).pipe(GulpIf(!workspace.onlyErrorNotification, notifier.success('All i18n files have been compiled successfully!')));
        });

        gulp.task('build-module', function () {
            return gulp.src('./package.json').pipe(Plumber({
                errorHandler: function errorHandler(error) {
                    notifier.error(error);
                }
            })).pipe(moduleBuilder.all()).pipe(GulpIf(!workspace.onlyErrorNotification, notifier.success('All module js files has been compiled successfully!')));
        });

        gulp.task('build-css', function () {
            return gulp.src('./package.json').pipe(Plumber({
                errorHandler: function errorHandler(error) {
                    notifier.error(error);
                }
            })).pipe(cssBuilder.all()).pipe(GulpIf(!workspace.onlyErrorNotification, notifier.success('All css files has been compiled successfully!')));
        });

        gulp.task('build-template', function () {
            return gulp.src('./package.json').pipe(Plumber({
                errorHandler: function errorHandler(error) {
                    notifier.error(error);
                }
            })).pipe(templateBuilder.all()).pipe(GulpIf(!workspace.onlyErrorNotification, notifier.success('All template files has been reloaded successfully!')));
        });

        gulp.task('build-image', function () {
            return gulp.src('./package.json').pipe(Plumber({
                errorHandler: function errorHandler(error) {
                    notifier.error(error);
                }
            })).pipe(assetBuilder.cleanupImage()).pipe(assetBuilder.buildImage()).pipe(GulpIf(!workspace.onlyErrorNotification, notifier.success('All image files has been reloaded successfully!')));
        });

        gulp.task('build-font', function () {
            return gulp.src('./package.json').pipe(Plumber({
                errorHandler: function errorHandler(error) {
                    notifier.error(error);
                }
            })).pipe(assetBuilder.cleanupFont()).pipe(assetBuilder.buildFont()).pipe(GulpIf(!workspace.onlyErrorNotification, notifier.success('All font files has been reloaded successfully!')));
        });

        gulp.task('build-extJs', function () {
            return gulp.src('./package.json').pipe(Plumber({
                errorHandler: function errorHandler(error) {
                    notifier.error(error);
                }
            })).pipe(assetBuilder.cleanupExtJs()).pipe(assetBuilder.buildExtJs()).pipe(GulpIf(!workspace.onlyErrorNotification, notifier.success('All Ext JS files has been reloaded successfully!')));
        });

        gulp.task('build-index.html', function () {
            return gulp.src('./package.json').pipe(Plumber({
                errorHandler: function errorHandler(error) {
                    notifier.error(error);
                }
            })).pipe(indexBuilder.cleanup()).pipe(indexBuilder.buildIndexHtml()).pipe(indexBuilder.buildIco()).pipe(GulpIf(!workspace.onlyErrorNotification, notifier.success('index.html file has been reloaded successfully!')));
        });

        gulp.task('build-application-constants', function () {
            return gulp.src('./package.json').pipe(Plumber({
                errorHandler: function errorHandler(error) {
                    notifier.error(error);
                }
            })).pipe(applicationBuilder.cleanup()).pipe(applicationBuilder.buildApplicationConstants()).pipe(GulpIf(!workspace.onlyErrorNotification, notifier.success('The application constants have been reloaded successfully!')));
        });

        gulp.task('build-module-constants', function () {
            return gulp.src('./package.json').pipe(Plumber({
                errorHandler: function errorHandler(error) {
                    notifier.error(error);
                }
            })).pipe(applicationBuilder.cleanup()).pipe(applicationBuilder.buildModuleConstants()).pipe(GulpIf(!workspace.onlyErrorNotification, notifier.success('The module constants have been reloaded successfully!')));
        });

        gulp.task('optimize-index.html', function () {
            return gulp.src('./package.json').pipe(optimizationBuilder.optimizeIndexHtml(settings));
        });

        gulp.task('optimize-build', function () {
            return gulp.src('./package.json').pipe(optimizationBuilder.optimizeBuild(settings));
        });

        gulp.task('service', function () {
            var service = new Service(options);

            return gulp.src('./package.json').pipe(service.start());
        });

        gulp.task('watch', function () {
            watch(i18nBuilder.getPath() + '/**/*.yaml', { read: false }, gulp.series('build-i18n'));
            watch(cssBuilder.getPath() + '/**/*.*', { read: false }, gulp.series('build-css'));
            watch(moduleBuilder.getPath() + '/**/*.js', { read: false }, gulp.series('build-module'));
            watch(templateBuilder.getPath() + '/**/*.*', { read: false }, gulp.series('build-template'));
            watch(assetBuilder.getImagePath() + '/**/*.*', { read: false }, gulp.series('build-image'));
            watch(assetBuilder.getFontPath() + '/**/*.*', { read: false }, gulp.series('build-font'));

            if (workspace.additionalBuild.extJs) {
                watch(assetBuilder.getExtJsPath() + '/**/*.*', { read: false }, gulp.series('build-extJs'));
            }

            if (workspace.additionalBuild.indexHtml) {
                watch(indexBuilder.getPath() + '/*.{ico,html}', { read: false }, gulp.series('build-index.html'));
                watch([resources.getPath() + '/application.yaml', resources.getProfilePath() + '/application.*.yaml'], { read: false }, gulp.series('build-application-constants'));
                watch([resources.getPath() + '/bootstrap.yaml', resources.getProfilePath() + '/bootstrap.*.yaml'], { read: false }, gulp.series('build-index.html'));
            }

            if (workspace.additionalBuild.module) {
                watch([resources.getPath() + '/module.yaml'], { read: false }, gulp.series('build-module-constants'));
            }
        });

        gulp.task('version', RunCommand('cag about'));

        gulp.task('start', gulp.series('version', 'init', 'build', 'service', 'watch'));

        gulp.task('package', gulp.series('version', 'init', 'build'));
    };
}

module.exports = GulpCagSPa;