var gulp = require('gulp');
var plugins = require('gulp-load-plugins')({
  pattern : [ '*' ],
  overridePattern : true,
  scope : [ 'devDependencies' ]
});

gulp.task('cleanup', function () {
  return gulp.src('dist/**', {read: false})
    .pipe(plugins.clean({force: true}));
});

gulp.task('build', function () {
  return gulp.src('dist/**/*.js')
    .pipe(plugins.babel({
      presets: ['env']
    }))
    .pipe(gulp.dest('dist'));
});

gulp.task('inject', function(){
  var target = gulp.src('src/index.js');
  var buildSources = gulp.src(['src/build/**/*.js'], {read: true});
  var commonSources = gulp.src(['src/common/**/*.js'], {read: true});
  var wwwSources = gulp.src(['src/www/**/*.js'], {read: true});
  
  return target.pipe(
      plugins.inject(buildSources, {
        relative: true,
        starttag: '/* inject:build:js */',
        endtag: '/* end:inject:build */',
        transform: function (filePath, file) {
          return file.contents.toString('utf8');
        }
      })
   )
   .pipe(
       plugins.inject(commonSources, {
         relative: true,
         starttag: '/* inject:common:js */',
         endtag: '/* end:inject:common */',
         transform: function (filePath, file) {
           return file.contents.toString('utf8');
         }
       })
   )
   .pipe(
       plugins.inject(wwwSources, {
         relative: true,
         starttag: '/* inject:www:js */',
         endtag: '/* end:inject:www */',
         transform: function (filePath, file) {
           return file.contents.toString('utf8');
         }
       })
   )
   .pipe(gulp.dest('dist'));
});

gulp.task('watch', function () {
  return plugins.watch('src/**/*.*', gulp.series('cleanup', 'inject', 'build'));
});

gulp.task('start', gulp.series('cleanup', 'inject', 'build', 'watch'));